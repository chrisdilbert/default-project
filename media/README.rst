This directory only contains user uploaded media. You should serve files from
this directory directly via your webserver (ie. not via the WSGI-handler).
