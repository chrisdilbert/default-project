Django==1.6
django-facebook==5.2.10
django-tastypie==0.10.0
-e git://github.com/garromark/django-tastypie-ext.git@24e02646d9958881ec34e58eaf2a859b49d719f6#egg=django_tastypie_ext-dev
-e git://github.com/divio/django-cms.git@e1a57fd584b470403b194b53ee7961c280d2d88a#egg=django-cms
South==0.8.3
Unidecode==0.04.14
psycopg2==2.5.1