# Create your views here.
import json
from django.http import HttpResponse
from django.views.generic import View




def outdatedApi(self, *args, **kwargs):
    resp = {'error': 'Outdated api please use api v2 ', 'code': '1'}
    return HttpResponse(json.dumps(resp), content_type="application/json" )